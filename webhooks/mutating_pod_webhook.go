/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package webhooks

import (
	"context"
	"encoding/json"
	"net/http"
	"path/filepath"

	"github.com/pkg/errors"

	corev1 "k8s.io/api/core/v1"

	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	"gitlab.com/jrr/vault-friend/pkg/util"
)

type PodMutator struct {
	client.Client
	decoder *admission.Decoder
}

// Handle implements admission.Handler and contains the logic necessary
// to add envFrom and volumeMount secrets to specified containers as
// they come in. There's a lot going on, so this might be too slow to
// use heavily. If that's the case, it'll need to be refactored somehow.
// +kubebuilder:webhook:path=/pod-mutator,mutating=true,failurePolicy=fail,groups="",resources=pods,verbs=create;update,versions=v1,name=podmut.vault-friend.jrr.io
func (pm *PodMutator) Handle(ctx context.Context, req admission.Request) admission.Response {
	pod := &corev1.Pod{}

	err := pm.decoder.Decode(req, pod)

	if err != nil {
		return admission.Errored(http.StatusBadRequest, errors.Wrap(err, "could not decode Pod"))
	}

	if len(pod.ObjectMeta.OwnerReferences) == 0 || len(pod.Spec.Containers) == 0 {
		// no owner, no containers? then we don't care
		return patchAndAdmit(req, pod)
	}

	// get ownerref (recurse up if needed)

	owner, err := util.GetSupportedOwner(ctx, pm.Client, req.Namespace, pod.ObjectMeta.OwnerReferences)

	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	if owner == nil {
		// no supported owner, admit unchanged
		return patchAndAdmit(req, pod)
	}

	listOptions := []client.ListOption{
		client.InNamespace(req.Namespace),
	}

	bindingList := &vf1a1.VaultBindingList{}

	err = pm.Client.List(ctx, bindingList, listOptions...)

	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	if len(bindingList.Items) == 0 {
		// pod owner is not associated with any bindings, admit unchanged
		return patchAndAdmit(req, pod)

	}

	var binding vf1a1.VaultBinding

	for _, item := range bindingList.Items {
		if string(item.Spec.TargetRef.Kind) == owner.Kind && item.Spec.TargetRef.Name == owner.Name {
			binding = item
			break
		}
	}

	if binding.Spec.TargetRef.Name == "" {
		// we didn't find a valid matching binding, so ignore this pod
		return patchAndAdmit(req, pod)
	}

	// if binding not OK, add condition

	if !binding.Status.OK {
		return admission.Errored(http.StatusInternalServerError, errors.New("binding is not in OK status"))
	}

	// get our secrets in namespace

	labelMatcher, _ := labels.NewRequirement(util.PrefixedLabel("parent.kind"), selection.Exists, []string{})
	labelSelector := labels.NewSelector().Add(*labelMatcher)

	secretListOptions := []client.ListOption{
		client.InNamespace(binding.ObjectMeta.Namespace),
		client.MatchingLabelsSelector{Selector: labelSelector},
	}

	secretList := &corev1.SecretList{}

	err = pm.Client.List(ctx, secretList, secretListOptions...)

	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	if len(secretList.Items) == 0 {
		return admission.Errored(http.StatusInternalServerError, errors.New("no Secrets found in namespace"))
	}

	// iterate over binding.resources as resource

	if pod.Spec.Volumes == nil {
		pod.Spec.Volumes = []corev1.Volume{}
	}

	for _, resource := range binding.Spec.Resources {
		var secret *corev1.Secret

		// get secret from list or err

		for _, secretItem := range secretList.Items {
			if string(resource.Kind) == secretItem.ObjectMeta.Labels[util.PrefixedLabel("parent.kind")] && resource.Name == secretItem.ObjectMeta.Labels[util.PrefixedLabel("parent.name")] {
				secret = &secretItem
				break
			}
		}

		if secret == nil {
			return admission.Errored(http.StatusInternalServerError, errors.New("no Secrets found for resource"))
		}

		for idx, container := range pod.Spec.Containers {
			if container.EnvFrom == nil {
				container.EnvFrom = []corev1.EnvFromSource{}
			}

			if container.VolumeMounts == nil {
				container.VolumeMounts = []corev1.VolumeMount{}
			}

			switch resource.Kind {
			case vf1a1.ResourceKindEnv:
				if itemInList(container.Name, binding.Spec.TargetRef.ContainerNames) {
					secretEnvSource := &corev1.SecretEnvSource{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: secret.ObjectMeta.Name,
						},
						Optional: util.BoolPtr(false),
					}

					envSource := corev1.EnvFromSource{
						SecretRef: secretEnvSource,
					}

					container.EnvFrom = append(container.EnvFrom, envSource)
				}
			case vf1a1.ResourceKindCert:
				secretVolumeSource := &corev1.SecretVolumeSource{
					SecretName:  secret.ObjectMeta.Name,
					DefaultMode: util.Int32Ptr(0444),
					Optional:    util.BoolPtr(false),
				}

				secretVolume := corev1.Volume{
					Name: resource.Name,
					VolumeSource: corev1.VolumeSource{
						Secret: secretVolumeSource,
					},
				}

				pod.Spec.Volumes = append(pod.Spec.Volumes, secretVolume)

				if itemInList(container.Name, binding.Spec.TargetRef.ContainerNames) {
					mountPath := filepath.Join("/var", "run", "secrets", "vault-friend.jrr.io", resource.Name)

					if resource.MountPath != "" {
						mountPath = resource.MountPath
					}

					volumeMount := corev1.VolumeMount{
						Name:      resource.Name,
						ReadOnly:  true,
						MountPath: mountPath,
					}

					container.VolumeMounts = append(container.VolumeMounts, volumeMount)
				}
			default:
				return admission.Errored(http.StatusInternalServerError, errors.Errorf("unsupported ResourceKind %s", resource.Kind))

			}

			pod.Spec.Containers[idx] = container
		}
	}

	return patchAndAdmit(req, pod)
}

func itemInList(name string, list []string) bool {
	if len(list) == 0 {
		return true
	}

	for _, item := range list {
		if name == item {
			return true
		}
	}

	return false
}

func patchAndAdmit(req admission.Request, pod *corev1.Pod) admission.Response {
	marshaledPod, err := json.Marshal(pod)

	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	return admission.PatchResponseFromRaw(req.Object.Raw, marshaledPod)
}

// InjectDecoder injects the decoder.
// Implements admission.DecoderInjector.
func (pm *PodMutator) InjectDecoder(d *admission.Decoder) error {
	pm.decoder = d
	return nil
}
