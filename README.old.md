# Vault-Friend

Because everybody can use a friend sometimes. The missing(?) glue
between Hashicorp Vault and Kubernetes.

## What it actually does

Vault-Friend is a Kubernetes operator that allows you to transparently
connect Pods with secret data from Hashicorp Vault. It does this with
CustomResources and a mutating admission webhook. Once you have set up a
VaultBinding and its dependencies, simply scaling up or restarting the
object you've targeted will cause it to submit new Pod objects which
will have the Vault data automatically attached to them.

## Setup

Sorry, but this can't be a class in connecting Kubernetes to Vault.

IF:

- You have `kubectl` and `kustomize`
- You have a Kubernetes cluster set up
- You have a Vault server or cluster running at e.g., `https://vault`
- You have a `Deployment` in Kubernetes called e.g., `example-app`
- You have configured Vault with a Kubernetes auth engine mounted at e.g., `kubernetes/`
- You have configured a role within `kubernetes/` for the `example-app`
- Your policies allow the `example-app`'s `ServiceAccount` to access the data you wish to use

THEN:

The manifests for installation can be built with `kustomize build config/default`.

If you've previously done all the above steps, Vault-Friend should
start up and start allowing you to create custom resources.

## Telling the webhook to ignore namespaces

A mutating admission webhook for all pods in all namespaces is
something that seems like it'd cause a lot of problems, right? Well it
can be. If you want the webhook to ignore pods from a particular
namespace, simply add the label `vault-friend.jrr.io/ignore: "true"` to
said namespace. Vault-Friend will thereafter not process pods from that
namespace and simply admit them without mutation. Removing the label
from the namespace will allow Vault-Friend to process pods from that
namespace once again, if desired.

## Custom Resources

These examples make the same assumptions as the [Setup section](#Setup).

Every custom resource also has a `.status` field that provides useful
information about the last observed state of the object and can help you
to pinpoint errors.

### Vault

`Vault` is intended as a normalized way to connect a single Kubernetes
cluster to multiple Vault instances or via multiple auth engines. Upon
creation or update, the controller will attempt to contact the Vault
server to see if it can be reached and its current seal status.

```yaml
---
apiVersion: vault-friend.jrr.io/v1alpha1
kind: Vault
metadata:
  name: vault
spec:
  address: "https://vault"
  authPath: "kubernetes"
```

### VaultCertificate

`VaultCertificate` is a resource for PKI engine secrets from Vault. It
supports SubjectAlternativeNames of various types, the default being a
`dns` type. The certificate and key will be mounted in
`/var/run/secrets/vault-friend.jrr.io/{short}` where `{short}` is the
last segment of the associated Kubernetes secret object's name by
default. This can be overridden by `.spec.mountPath`.

```yaml
---
apiVersion: vault-friend.jrr.io/v1alpha1
kind: VaultCertificate
metadata:
  name: example-app-cert
  namespace: default
spec:
  pkiEnginePath: pki
  role: example-app-server
  commonName: example-app.default.svc
  ttl: 160h
  subjectAltNames:
  - name: example-app.internal.company.com
  - type: ip
    name: "192.168.1.100"
```

### VaultEnvironment

**Note:** Only version 2 of the Vault KV engine is supported.

`VaultEnvironment` fetches data from a Vault KV path and will be
converted to an `EnvFromSource` on targeted pods.

```yaml
---
apiVersion: vault-friend.jrr.io/v1alpha1
kind: VaultEnvironment
metadata:
  name: example-app-env
  namespace: default
spec:
  path: "kv/data/example-app-envvars"
  pinVersion: 3
  envPrefix: ""
  capitalizeKeys: true
```

### VaultBinding

`VaultBinding` is what ties the secret data to a Vault server and then to
downstream Pods. Creating or modifying a VaultBinding initiates the
reconciliation process that fetches environments and issues certs. The
object referred to in `targetRef` must exist before the VaultBinding
can successfully fetch and store secrets from Vault.

It's worth noting that VaultBindings should be secured against
unauthorized creation or modification. They contain no secret
information, but they are the glue that ties everything together.
Policy is still enforced at Vault - that is, if a malicious VaultBinding
tried to target a StatefulSet, requesting secrets that the StatefulSet's
service account is not allowed to access, Vault-Friend will not be able
to fetch their data from Vault and will also not attach any data from
existing secrets to the pods.

```yaml
---
apiVersion: vault-friend.jrr.io/v1alpha1
kind: VaultBinding
metadata:
  name: example-app
  namespace: default
spec:
  vaultName: vault
  vaultRole: example-app
  targetRef:
    kind: Deployment
    name: example-app
  resources:
  - kind: VaultCertificate
    name: example-app-cert
  - kind: VaultEnvironment
    name: example-app-env
```

## FAQ

**How do I know I can trust this software with my secrets?**

_Don't_ trust me. Seriously, it's a bad idea. Read the code instead.
Help me make it more secure if you want to.

**The webhook isn't letting `kube-system` pods through! HELP!**

You should check the "Telling the webhook to ignore namespaces"
section of this document. It's near the top.

**Will putting a mutating webhook in front of all pods slow down my
cluster?**

It has the potential to do so. There is very little optimization
thus far in the project, so I encourage trying and finding out.
