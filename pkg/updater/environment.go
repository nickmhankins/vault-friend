package updater

import (
	"encoding/json"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	vaultApi "github.com/hashicorp/vault/api"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	"gitlab.com/jrr/vault-friend/pkg/util"
)

func (r *Updater) UpdateEnvironment(env *vf1a1.VaultEnvironment) error {
	var vaultSecret *vaultApi.Secret

	// get secret
	secretList := &corev1.SecretList{}
	listOpts := util.SecretListOptions(env.Kind, env.ObjectMeta)

	err := r.kubeClient.List(r.ctx, secretList, listOpts...)

	if err != nil {
		return errors.Wrap(err, "could not get Kubernetes secrets")
	}

	if len(secretList.Items) != 1 {
		return errors.Errorf("expected to find 1 k8s secret, found %d", len(secretList.Items))
	}

	kubeSecret := secretList.Items[0].DeepCopy()

	// get vault path

	if env.Spec.PinVersion != 0 {
		readData := map[string][]string{
			"version": []string{strconv.Itoa(env.Spec.PinVersion)},
		}

		vaultSecret, err = r.vaultClient.Logical().ReadWithData(env.Spec.Path, readData)
	} else {
		vaultSecret, err = r.vaultClient.Logical().Read(env.Spec.Path)
	}

	if err != nil {
		return errors.Wrap(err, "could not fetch data from Vault path")
	}

	vaultMetadata := vaultSecret.Data["metadata"].(map[string]interface{})
	vaultSecretVersion, err := strconv.Atoi(string(vaultMetadata["version"].(json.Number)))

	if err != nil {
		return errors.Wrap(err, "could not parse Vault metadata")
	}

	// this is the second part of the update checking logic - if the
	// version we got is the same as the version we have, nothing to do
	if !env.Spec.AlwaysUpdate && vaultSecretVersion == env.Status.Version {
		return nil
	}

	// transform and store vars

	vaultData := vaultSecret.Data["data"].(map[string]interface{})

	if env.Spec.EnvPrefix != "" {
		for oldKey, val := range vaultData {
			newKey := env.Spec.EnvPrefix + oldKey

			delete(vaultData, oldKey)

			vaultData[newKey] = val
		}
	}

	if env.Spec.CapitalizeKeys {
		for oldKey, val := range vaultData {
			newKey := strings.ToUpper(oldKey)

			delete(vaultData, oldKey)

			vaultData[newKey] = val
		}
	}

	if kubeSecret.Data == nil {
		kubeSecret.Data = make(map[string][]byte)
	}

	for key, val := range vaultData {
		kubeSecret.Data[key] = []byte(val.(string))
	}

	err = r.kubeClient.Update(r.ctx, kubeSecret)

	if err != nil {
		return errors.Wrap(err, "could not persist k8s secret data")
	}

	// update env status

	now := metav1.Now()

	envStatus := vf1a1.VaultEnvironmentStatus{
		OK:            true,
		Message:       "ok",
		Version:       vaultSecretVersion,
		LastFetchTime: &now,
	}

	env.Status = envStatus

	err = r.kubeClient.Status().Update(r.ctx, env)

	if err != nil {
		return errors.Wrap(err, "could not update VaultEnvironment status")
	}

	return nil
}
