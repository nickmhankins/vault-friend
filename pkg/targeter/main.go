package targeter

import (
	"context"

	"github.com/pkg/errors"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	batchv1b1 "k8s.io/api/batch/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	"gitlab.com/jrr/vault-friend/pkg/util"
)

type Targeter struct {
	ctx        context.Context
	kubeClient client.Client
	namespace  string
}

type TargetObject struct {
	LabelSelector   *metav1.LabelSelector
	NamespacedName  types.NamespacedName
	PodTemplateSpec *corev1.PodTemplateSpec
}

func NewTargeter(ctx context.Context, kubeClient client.Client, namespace string) *Targeter {
	return &Targeter{
		ctx:        ctx,
		kubeClient: kubeClient,
		namespace:  namespace,
	}
}

func (t *Targeter) OwnerRef(ownerRef *metav1.OwnerReference) (*TargetObject, error) {
	var targetKind vf1a1.TargetKind

	switch ownerRef.Kind {
	case "CronJob":
		targetKind = vf1a1.TargetKindCronJob
	case "DaemonSet":
		targetKind = vf1a1.TargetKindDaemonSet
	case "Deployment":
		targetKind = vf1a1.TargetKindDeployment
	case "Job":
		targetKind = vf1a1.TargetKindJob
	case "StatefulSet":
		targetKind = vf1a1.TargetKindStatefulSet
	}

	targetRef := &vf1a1.TargetReference{
		Kind: targetKind,
		Name: ownerRef.Name,
	}

	return t.TargetRef(targetRef)
}

// targetObject converts a supported object into a TargetObject for easier handling.
func (t *Targeter) TargetRef(targetRef *vf1a1.TargetReference) (*TargetObject, error) {
	var err error

	targetObject := &TargetObject{}

	targetObject.NamespacedName = util.NamespacedName(t.namespace, targetRef.Name)

	switch targetRef.Kind {
	case vf1a1.TargetKindCronJob:
		target := &batchv1b1.CronJob{}

		err = t.kubeClient.Get(t.ctx, targetObject.NamespacedName, target)

		if err != nil {
			return nil, errors.Wrapf(err, "could not fetch target %s: %s", targetRef.Kind, targetObject.NamespacedName)
		}

		targetObject.LabelSelector = target.Spec.JobTemplate.Spec.Selector
		targetObject.PodTemplateSpec = &target.Spec.JobTemplate.Spec.Template
	case vf1a1.TargetKindDaemonSet:
		target := &appsv1.DaemonSet{}

		err = t.kubeClient.Get(t.ctx, targetObject.NamespacedName, target)

		if err != nil {
			return nil, errors.Wrapf(err, "could not fetch target %s: %s", targetRef.Kind, targetObject.NamespacedName)
		}

		targetObject.LabelSelector = target.Spec.Selector
		targetObject.PodTemplateSpec = &target.Spec.Template
	case vf1a1.TargetKindDeployment:
		target := &appsv1.Deployment{}

		err = t.kubeClient.Get(t.ctx, targetObject.NamespacedName, target)

		if err != nil {
			return nil, errors.Wrapf(err, "could not fetch target %s: %s", targetRef.Kind, targetObject.NamespacedName)
		}

		targetObject.LabelSelector = target.Spec.Selector
		targetObject.PodTemplateSpec = &target.Spec.Template
	case vf1a1.TargetKindJob:
		target := &batchv1.Job{}

		err = t.kubeClient.Get(t.ctx, targetObject.NamespacedName, target)

		if err != nil {
			return nil, errors.Wrapf(err, "could not fetch target %s: %s", targetRef.Kind, targetObject.NamespacedName)
		}

		targetObject.LabelSelector = target.Spec.Selector
		targetObject.PodTemplateSpec = &target.Spec.Template
	case vf1a1.TargetKindStatefulSet:
		target := &appsv1.StatefulSet{}

		err = t.kubeClient.Get(t.ctx, targetObject.NamespacedName, target)

		if err != nil {
			return nil, errors.Wrapf(err, "could not fetch target %s: %s", targetRef.Kind, targetObject.NamespacedName)
		}

		targetObject.LabelSelector = target.Spec.Selector
		targetObject.PodTemplateSpec = &target.Spec.Template
	default:
		return nil, errors.Errorf("unsupported target Kind %s", targetRef.Kind)
	}

	if targetObject.PodTemplateSpec.Spec.ServiceAccountName == "" {
		return nil, errors.Errorf("target %s has no ServiceAccountName", targetRef.Name)
	}

	return targetObject, nil
}
