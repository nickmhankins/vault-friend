/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package controllers

import (
	"context"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"

	corev1 "k8s.io/api/core/v1"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	util "gitlab.com/jrr/vault-friend/pkg/util"
)

// VaultEnvironmentReconciler reconciles a VaultEnvironment object
type VaultEnvironmentReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaultenvironments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaultenvironments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete

func (r *VaultEnvironmentReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := r.Log.WithValues("vaultEnvironment", req.NamespacedName)

	logger.Info("reconciling VaultEnvironment")

	env := &vf1a1.VaultEnvironment{}
	err := r.Get(ctx, req.NamespacedName, env)

	if kerrors.IsNotFound(err) {
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not fetch VaultEnvironment")
	}

	if env.ObjectMeta.DeletionTimestamp.IsZero() {
		secretList := &corev1.SecretList{}

		err = r.List(ctx, secretList, util.SecretListOptions(env.Kind, env.ObjectMeta)...)

		if err != nil {
			return ctrl.Result{}, errors.Wrapf(err, "could not list Secrets in namespace %s", env.ObjectMeta.Namespace)
		}

		if secretsFound := len(secretList.Items); secretsFound == 0 {
			err = r.Create(ctx, util.DataSecret(env.TypeMeta, env.ObjectMeta))

			if err != nil {
				return ctrl.Result{}, errors.Wrapf(err, "could not create Secret for VaultEnvironment %s", env.ObjectMeta.Name)
			}

		} else if secretsFound > 1 {

			return ctrl.Result{}, errors.Errorf("found too many Secrets (%d) owned by this VaultEnvironment", secretsFound)

		}
	} else {
		secret := &corev1.Secret{}

		err = r.DeleteAllOf(ctx, secret, util.SecretDeleteAllOfOptions(env.Kind, env.ObjectMeta)...)

		if err != nil {
			logger.Error(err, "could not delete associated Secret, k8s GC will get it")
		}

	}

	return ctrl.Result{}, nil
}

func (r *VaultEnvironmentReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vf1a1.VaultEnvironment{}).
		Complete(r)
}
