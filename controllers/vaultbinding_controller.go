/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package controllers

import (
	"context"
	"path"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	tgt "gitlab.com/jrr/vault-friend/pkg/targeter"
	"gitlab.com/jrr/vault-friend/pkg/updater"
	"gitlab.com/jrr/vault-friend/pkg/util"
)

type TargetObject struct {
	LabelSelector   *metav1.LabelSelector
	NamespacedName  types.NamespacedName
	PodTemplateSpec *corev1.PodTemplateSpec
}

// VaultBindingReconciler reconciles a VaultBinding object
type VaultBindingReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

var logger logr.Logger

// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaultbindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaultbindings/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;update
// +kubebuilder:rbac:groups=apps,resources=daemonsets;deployments;replicasets;statefulsets,verbs=get;list;watch
// +kubebuilder:rbac:groups=batch,resources=cronjobs;jobs,verbs=get;list;watch

func (r *VaultBindingReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var tokenSecret *corev1.Secret

	logger = r.Log.WithValues("vaultBinding", req.NamespacedName)

	logger.Info("reconciling VaultBinding")

	binding := &vf1a1.VaultBinding{}
	err := r.Get(ctx, req.NamespacedName, binding)

	if kerrors.IsNotFound(err) {
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not fetch VaultBinding")
	}

	// object is being deleted, nothing to to
	if !binding.ObjectMeta.DeletionTimestamp.IsZero() {
		return ctrl.Result{}, nil
	}

	targeter := tgt.NewTargeter(ctx, r.Client, req.NamespacedName.Namespace)

	targetObject, err := targeter.TargetRef(&binding.Spec.TargetRef)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not convert targetRef to useful object")
		return ctrl.Result{}, errors.Wrap(err, "could not convert targetRef to useful object")
	}

	// - get target's SA token secret
	//   - err? => status, log, requeue

	serviceAccountName := targetObject.PodTemplateSpec.Spec.ServiceAccountName
	tokenSecrets := &corev1.SecretList{}

	listOpts := []client.ListOption{
		client.InNamespace(req.NamespacedName.Namespace),
	}

	err = r.List(ctx, tokenSecrets, listOpts...)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not get ServiceAccount token secrets", "namespace", req.NamespacedName.Namespace)
		return ctrl.Result{}, errors.Wrapf(err, "could not get ServiceAccount token secrets in namespace %s", req.NamespacedName.Namespace)
	}

	if len(tokenSecrets.Items) == 0 {
		err = errors.New("no token secrets found in namespace")
		r.errorStatus(binding, err)
		logger.Error(err, "could not get ServiceAccount token secrets", "namespace", req.NamespacedName.Namespace)
		return ctrl.Result{}, errors.Wrapf(err, "could not get ServiceAccount token secrets in namespace %s", req.NamespacedName.Namespace)
	}

	for _, item := range tokenSecrets.Items {
		annotations := item.ObjectMeta.Annotations

		if item.Type != corev1.SecretTypeServiceAccountToken {
			continue
		}

		if annotations == nil {
			continue
		}

		if annotations[corev1.ServiceAccountNameKey] == serviceAccountName {
			tokenSecret = &item
			break
		}
	}

	if tokenSecret == nil {
		err = errors.New("no token secrets found for target")
		r.errorStatus(binding, err)
		logger.Error(err, "could not find token", "serviceAccount", serviceAccountName)
		return ctrl.Result{}, errors.Wrapf(err, "could not find token for serviceAccount %s", serviceAccountName)
	}

	serviceAccountToken := string(tokenSecret.Data["token"])

	// - fetch vault
	//   - err? => status, log, requeue

	vaultObj := &vf1a1.Vault{}

	vaultNSN := util.NamespacedName("", binding.Spec.VaultName)

	err = r.Get(ctx, vaultNSN, vaultObj)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not find Vault server data")
		return ctrl.Result{}, errors.Wrap(err, "could not find Vault server data")
	}

	// - create vault client
	//   - err? => status, log, requeue

	vaultClient, err := util.VaultClient(vaultObj)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not create Vault client")
		return ctrl.Result{}, errors.Wrap(err, "could not create Vault client")
	}

	// - auth to vault
	//   - err? => status, log, requeue

	vaultAuthData := map[string]interface{}{
		"role": binding.Spec.VaultRole,
		"jwt":  serviceAccountToken,
	}

	vaultAuthPath := path.Join("auth", vaultObj.Spec.AuthPath, "login")

	vaultTokenSecret, err := vaultClient.Logical().Write(vaultAuthPath, vaultAuthData)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not authenticate to Vault")
		return ctrl.Result{}, errors.Wrap(err, "could not authenticate to Vault")
	}

	// - set vault token

	vaultClient.SetToken(vaultTokenSecret.Auth.ClientToken)

	// - make status slice

	resourceStatuses := []vf1a1.ResourceStatus{}

	allOK := true

	// - for each secret

	objectUpdater := updater.NewUpdater(r.Client, logger, vaultClient)

	for _, resource := range binding.Spec.Resources {
		resourceStatus := vf1a1.ResourceStatus{
			Kind:    resource.Kind,
			Name:    resource.Name,
			OK:      true,
			Message: "ok",
		}

		switch resource.Kind {
		case vf1a1.ResourceKindEnv:
			env := &vf1a1.VaultEnvironment{}
			err = r.Get(ctx, util.NamespacedName(req.NamespacedName.Namespace, resource.Name), env)

			if err != nil {
				resourceStatus = resourceStatusFail(resourceStatus, err)
				break
			}

			if !env.NeedsUpdate() {
				break
			}

			err = objectUpdater.UpdateEnvironment(env)

			if err != nil {
				resourceStatus = resourceStatusFail(resourceStatus, err)
				break
			}
		}

		if !resourceStatus.OK {
			allOK = false
		}

		resourceStatuses = append(resourceStatuses, resourceStatus)
	}

	if !allOK {
		binding.Status = statusObject(false, "error during resource processing", resourceStatuses...)
	} else {
		binding.Status = statusObject(true, "ok", resourceStatuses...)
	}

	err = r.Update(ctx, binding)

	if err != nil {
		r.errorStatus(binding, err)
		logger.Error(err, "could not update VaultBinding")
		return ctrl.Result{}, errors.Wrap(err, "could not update VaultBinding")
	}

	err = r.Status().Update(ctx, binding)

	if err != nil {
		logger.Error(err, "could not update VaultBinding status")
		return ctrl.Result{}, errors.Wrap(err, "could not update VaultBinding status")
	}

	if !allOK {
		return ctrl.Result{}, errors.New("error during resource processing")
	}

	return ctrl.Result{}, nil
}

func resourceStatusFail(status vf1a1.ResourceStatus, err error) vf1a1.ResourceStatus {
	status.OK = false
	status.Message = err.Error()
	return status
}

func (r *VaultBindingReconciler) errorStatus(binding *vf1a1.VaultBinding, err error) {
	ctx := context.TODO()

	statusObj := statusObject(false, err.Error())

	binding.Status = statusObj

	logger.V(0).Info("putting binding in error status")

	_ = r.Status().Update(ctx, binding)
}

func statusObject(ok bool, msg string, resources ...vf1a1.ResourceStatus) vf1a1.VaultBindingStatus {
	return vf1a1.VaultBindingStatus{
		OK:        ok,
		Message:   msg,
		Resources: resources,
	}
}

func (r *VaultBindingReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vf1a1.VaultBinding{}).
		Complete(r)
}
