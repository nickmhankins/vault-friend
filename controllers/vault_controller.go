/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package controllers

import (
	"context"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
	util "gitlab.com/jrr/vault-friend/pkg/util"
)

// VaultReconciler reconciles a Vault object
type VaultReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaults,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=vaults/status,verbs=get;update;patch

func (r *VaultReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := r.Log.WithValues("vault", req.NamespacedName)

	logger.Info("reconciling Vault")

	vault := &vf1a1.Vault{}
	err := r.Get(ctx, req.NamespacedName, vault)

	if kerrors.IsNotFound(err) {
		// if we get a 404 at the top, just let it go
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not fetch Vault resource")
	}

	if !vault.ObjectMeta.DeletionTimestamp.IsZero() {
		// object is being deleted, nothing to do
		return ctrl.Result{}, nil
	}

	vaultClient, err := util.VaultClient(vault)

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not create Vault client")
	}

	vault.Status.Connected = false

	sealStatus, sealErr := vaultClient.Sys().SealStatus()

	if sealErr != nil {
		vault.Status.Message = sealErr.Error()
	} else {
		vault.Status.Connected = true
		vault.Status.Sealed = sealStatus.Sealed
		vault.Status.Message = "ok"
	}

	err = r.Status().Update(ctx, vault)

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not update Vault resource status")
	}

	return ctrl.Result{}, nil
}

func (r *VaultReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vf1a1.Vault{}).
		Complete(r)
}
