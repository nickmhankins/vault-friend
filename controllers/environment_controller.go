/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"

	vaultApi "github.com/hashicorp/vault/api"

	authenticationv1 "k8s.io/api/authentication/v1"
	corev1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/utils/ptr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a2 "gitlab.com/jrr/vault-friend/api/v1alpha2"
	util "gitlab.com/jrr/vault-friend/pkg/util"
)

var (
	saCache = make(serviceAccountTokenCache)
)

// EnvironmentReconciler reconciles a Environment object
type EnvironmentReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type serviceAccountTokenCache map[string]authenticationv1.TokenRequestStatus

func tokenHoursLeft(token authenticationv1.TokenRequestStatus) float64 {
	return time.Until(token.ExpirationTimestamp.Time).Hours()
}

// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=environments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=vault-friend.jrr.io,resources=environments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete

func (r *EnvironmentReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := r.Log.WithValues("environment", req.NamespacedName)
	logger.Info("reconciling Environment")

	environment := &vf1a2.Environment{}
	err := r.Get(ctx, req.NamespacedName, environment)

	if kerrors.IsNotFound(err) {
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "error while fetching Environment")
	}

	if !environment.ObjectMeta.DeletionTimestamp.IsZero() {
		// object is being deleted, nothing to do
		return ctrl.Result{}, nil
	}

	// if the object was annotated for a force refresh, delete the annotation,
	// update the object, and requeue it in a few seconds for further
	// reconciliation
	if _, ok := environment.ObjectMeta.Annotations[util.PrefixedLabel(vf1a2.ForceRefreshKey)]; ok {
		delete(environment.ObjectMeta.Annotations, util.PrefixedLabel(vf1a2.ForceRefreshKey))

		err = r.Update(ctx, environment)

		if err != nil {
			return ctrl.Result{}, errors.Wrap(err, "could not delete force-refresh annotation")
		} else {
			return ctrl.Result{RequeueAfter: (time.Second * 5)}, nil
		}
	}

	envPatch := client.MergeFrom(environment.DeepCopy())

	// establish lastTouch time
	timeNow := metav1.Now()
	environment.Status.LastTouch = &timeNow
	logger.V(3).Info("setting lastTouch", "lastTouch", timeNow.String())

	// establish requeue interval
	var requeueInterval time.Duration
	if environment.Spec.CheckInterval != "" {
		requeueInterval, err = time.ParseDuration(environment.Spec.CheckInterval)
	} else {
		requeueInterval, err = time.ParseDuration("15m")
	}

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "error while establishing Environment requeue interval")
	}

	logger.V(3).Info("requeue", "interval", requeueInterval)

	// err if secretName exists and is not owned

	secretExists := true
	secretObjectKey := util.NamespacedName(req.NamespacedName.Namespace, environment.Spec.SecretName)
	secretObject := &corev1.Secret{}
	err = r.Get(ctx, secretObjectKey, secretObject)

	if err != nil && kerrors.IsNotFound(err) {
		secretExists = false
	} else if err != nil && !kerrors.IsNotFound(err) {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "error while trying to find associated Secret")
	}

	secretIsOwned := false
	secretOwners := secretObject.ObjectMeta.OwnerReferences

	if secretExists {
		logger.V(3).Info("found named secret", "secret.name", secretObject.ObjectMeta.Name)

		if len(secretOwners) > 0 {
			for _, ownerRef := range secretOwners {
				if ownerRef.Kind == environment.TypeMeta.Kind && ownerRef.Name == environment.ObjectMeta.Name {
					secretIsOwned = true
				}
			}
		}

		logger.V(3).Info("do we own this Secret?", "secret.isOwned", secretIsOwned)

		if !secretIsOwned {
			err = errors.Errorf("found existing Secret %s not owned by this controller", secretObject.ObjectMeta.Name)
			environment.Status.Message = err.Error()
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{}, errors.Wrap(err, "error while trying to find associated Secret")
		}
	} else {
		logger.V(3).Info("didn't find named secret, setting one up", "secret.name", secretObject.ObjectMeta.Name)

		secretObject = &corev1.Secret{
			Type: corev1.SecretType(environment.Spec.SecretType),
			ObjectMeta: metav1.ObjectMeta{
				Namespace: environment.ObjectMeta.Namespace,
				Name:      environment.Spec.SecretName,
				Labels: map[string]string{
					util.PrefixedLabel("parent.kind"): "Environment",
					util.PrefixedLabel("parent.name"): environment.ObjectMeta.Name,
				},
				OwnerReferences: []metav1.OwnerReference{
					util.OwnerRef(environment.TypeMeta, environment.ObjectMeta),
				},
			},
		}
	}

	// get datasource, err if: !connected || !unsealed

	var (
		dataSourceSpec   vf1a2.DataSourceSpec
		dataSourceStatus vf1a2.DataSourceStatus
	)

	dataSourceRef := environment.Spec.DataSourceRef

	switch dataSourceRef.Kind {
	case "ClusterDataSource":
		logger.V(3).Info("kind: ClusterDataSource", "sourceRef.name", dataSourceRef.Name)

		dataSource := &vf1a2.ClusterDataSource{}

		sourceKey := util.NamespacedName("", environment.Spec.DataSourceRef.Name)
		logger.V(3).Info("fetching dataSource", "sourceRef.key", sourceKey)
		err = r.Get(ctx, sourceKey, dataSource)

		if err != nil {
			environment.Status.Message = err.Error()
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{}, errors.Wrap(err, "error while validating dataSourceRef")
		}

		dataSourceSpec = dataSource.Spec
		dataSourceStatus = dataSource.Status
	case "DataSource":
		logger.V(3).Info("kind: DataSource", "sourceRef.name", dataSourceRef.Name)

		dataSource := &vf1a2.DataSource{}

		sourceKey := util.NamespacedName(req.NamespacedName.Namespace, environment.Spec.DataSourceRef.Name)
		logger.V(3).Info("fetching dataSource", "sourceRef.key", sourceKey)
		err = r.Get(ctx, sourceKey, dataSource)

		if err != nil {
			environment.Status.Message = err.Error()
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{}, errors.Wrap(err, "error while validating dataSourceRef")
		}

		dataSourceSpec = dataSource.Spec
		dataSourceStatus = dataSource.Status
	default:
		err = errors.Errorf("unknown .dataSourceRef.kind %s", dataSourceRef.Kind)
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, err
	}

	logger.V(3).Info("dataSourceRef found", "kind", dataSourceRef.Kind, "name", dataSourceRef.Name)

	if !dataSourceStatus.Connected || !dataSourceStatus.Unsealed {
		err = errors.Errorf("Vault from %s %s is not connected or is sealed", dataSourceRef.Kind, dataSourceRef.Name)
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, err
	}

	// get Vault metadata

	logger.V(3).Info("setting up vault metadata client")

	metadataClient, err := dataSourceSpec.MetadataClient()

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "error while fetching Vault metadata")
	}

	logger.V(3).Info("reading vault metadata", "metadataPath", util.KVMetadataPath(environment.Spec.SourcePath.KV))

	vaultMetadata, err := metadataClient.Logical().Read(util.KVMetadataPath(environment.Spec.SourcePath.KV))

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "error while fetching Vault metadata")
	}

	// Check if the vaultMetadata or vaultMetadata.Data field is actually set, if not, error out
	// It seems that 404s from Vault are not an error condition above, so that can result in
	// undesired SIGSEGV panics if we don't check for these two conditions
	if vaultMetadata == nil || vaultMetadata.Data == nil {
		environment.Status.Message = "Data returned from Vault is nil (Does the KV path exist in Vault?)"
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.New(environment.Status.Message)
	}

	// if update available, set status field
	vaultMetadataVersion, keyOk := vaultMetadata.Data["current_version"]

	if !keyOk {
		err = errors.New("latest version could not be read")
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not parse Vault metadata")
	}

	vaultMetadataVersionInt, err := vaultMetadataVersion.(json.Number).Int64()

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not parse Vault metadata")
	}

	logger.V(3).Info("got vault metadata", "version", vaultMetadataVersionInt)

	if int64(environment.Status.Version) != vaultMetadataVersionInt {
		environment.Status.UpdateAvailable = true
	}

	logger.V(3).Info("update available?", "flag", environment.Status.UpdateAvailable)

	// here we decide whether to call it good or sync from Vault

	if environment.Spec.PinVersion != 0 {
		logger.V(3).Info("we have a pinned version", "pinVersion", environment.Spec.PinVersion)

		if environment.Status.Version == environment.Spec.PinVersion {
			logger.Info("Environment successfully reconciled")
			logger.V(3).Info(".spec.pinVersion matches .status.version, no update needed")
			environment.Status.Message = "ok"
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{RequeueAfter: requeueInterval}, nil
		}
	} else {
		logger.V(3).Info("no version pin", "alwaysUpdate?", environment.Spec.AlwaysUpdate)

		if !environment.Spec.AlwaysUpdate && !environment.Status.UpdateAvailable {
			logger.Info("Environment successfully reconciled")
			logger.V(3).Info(".spec.alwaysUpdate and .status.updateAvailable are both false, no update needed")
			environment.Status.Message = "ok"
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{RequeueAfter: requeueInterval}, nil
		}
	}

	// request serviceaccount token
	var serviceAccount corev1.ServiceAccount

	logger.V(3).Info("retrieving serviceaccount", "serviceAccount", environment.Spec.ServiceAccountName)

	err = r.Get(ctx, client.ObjectKey{Name: environment.Spec.ServiceAccountName, Namespace: req.NamespacedName.Namespace}, &serviceAccount)
	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not get serviceaccount")
	}

	cachedTokenName := fmt.Sprintf("%s/%s", req.NamespacedName.Namespace, environment.Spec.ServiceAccountName)

	token, exists := saCache[cachedTokenName]
	newTokenNeeded := false

	if !exists {
		newTokenNeeded = true
		logger.V(3).Info("serviceaccount token does not exist in cache, requesting token", "serviceaccount", environment.Spec.ServiceAccountName)
	}

	// renew if less than 48 hours remaining before expiry
	if exists && (tokenHoursLeft(token) < 48) {
		newTokenNeeded = true
		logger.V(3).Info("serviceaccount token exists in cache but is in the renewal window, requesting new token", "hoursRemaining", tokenHoursLeft(token))
		delete(saCache, cachedTokenName)
	}

	if newTokenNeeded {
		tr := authenticationv1.TokenRequest{
			Spec: authenticationv1.TokenRequestSpec{
				ExpirationSeconds: ptr.To(int64(604800)), // 1 week
			},
		}

		if err := r.SubResource("token").Create(ctx, &serviceAccount, &tr); err != nil {
			environment.Status.Message = err.Error()
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{}, err
		}
		token = tr.Status
		saCache[cachedTokenName] = token
	}

	logger.V(3).Info("current serviceacccount tokens cached", "cacheSize", len(saCache))

	logger.V(3).Info("serviceaccount token expiry", "expiresInHours", tokenHoursLeft(token))

	// setup Vault data client
	logger.V(3).Info("setting up vault client with serviceaccount token")
	secretClient, err := dataSourceSpec.AuthenticatedClient(dataSourceRef.AuthRole, token.Token)

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not create Vault client for sync")
	}

	// fetch vault data, otherwise err
	var vaultSecret *vaultApi.Secret

	if environment.Spec.PinVersion != 0 {
		readData := map[string][]string{
			"version": []string{strconv.Itoa(environment.Spec.PinVersion)},
		}

		logger.V(3).Info("fetching versioned data from vault", "path", environment.Spec.SourcePath.KV, "version", environment.Spec.PinVersion)

		vaultSecret, err = secretClient.Logical().ReadWithData(environment.Spec.SourcePath.KV, readData)
	} else {
		logger.V(3).Info("fetching latest data from vault", "path", environment.Spec.SourcePath.KV)

		vaultSecret, err = secretClient.Logical().Read(environment.Spec.SourcePath.KV)
	}

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not fetch data from Vault")
	}

	// transform and store vars

	logger.V(3).Info("starting data transforms")

	vaultSecretData := vaultSecret.Data["data"].(map[string]interface{})
	vaultSecretMetadata := vaultSecret.Data["metadata"].(map[string]interface{})

	if environment.Spec.KeyPrefix != "" {
		logger.V(3).Info("adding keyPrefix", "prefix", environment.Spec.KeyPrefix)

		for oldKey, val := range vaultSecretData {
			newKey := environment.Spec.KeyPrefix + oldKey

			delete(vaultSecretData, oldKey)

			vaultSecretData[newKey] = val
		}
	}

	if environment.Spec.CapitalizeKeys {
		logger.V(3).Info("capitalizing keys")

		for oldKey, val := range vaultSecretData {
			newKey := strings.ToUpper(oldKey)

			delete(vaultSecretData, oldKey)

			vaultSecretData[newKey] = val
		}
	}

	// put vault data into secret

	if secretObject.Data == nil {
		logger.V(3).Info("making empty secret.data")
		secretObject.Data = make(map[string][]byte)
	}

	for key, val := range vaultSecretData {
		switch v := val.(type) {
		case string:
			secretObject.Data[key] = []byte(v)
		default:
			environment.Status.Message = fmt.Sprintf("Vault secret data contains an unsupported type in key \"%s\": %T", key, v)
			_ = r.Status().Patch(ctx, environment, envPatch)
			return ctrl.Result{}, fmt.Errorf(environment.Status.Message)
		}
	}

	for key, _ := range secretObject.Data {
		if _, keyOk := vaultSecretData[key]; !keyOk {
			delete(secretObject.Data, key)
		}
	}

	if secretExists {
		logger.V(3).Info("updating secretObject with Vault data", "secret.name", secretObject.ObjectMeta.Name)

		err = r.Update(ctx, secretObject)
	} else {
		logger.V(3).Info("creating secretObject with Vault data", "secret.name", secretObject.ObjectMeta.Name)

		err = r.Create(ctx, secretObject)
	}

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not store data in Kubernetes")
	}
	logger.V(3).Info("successfully stored secret data", "secret.name", secretObject.ObjectMeta.Name)

	// quick secret metadata lookup

	vaultSecretVersion, keyOk := vaultSecretMetadata["version"]
	if !keyOk {
		err = errors.New("Vault secret metadata is missing key 'version'")
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not update Environment status after sync")
	}

	vaultSecretVersionInt, err := vaultSecretVersion.(json.Number).Int64()

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not parse Vault metadata")
	}

	logger.V(3).Info("read vault secret metadata", "version", vaultSecretVersionInt)

	// final status update

	environment.Status.UpdateAvailable = false
	if vaultSecretVersionInt != vaultMetadataVersionInt {
		environment.Status.UpdateAvailable = true
	}

	environment.Status.LastSync = &timeNow
	environment.Status.Message = "synced"
	environment.Status.Version = int(vaultSecretVersionInt)

	logger.V(3).Info("everything worked, status update", "status", environment.Status)

	err = r.Status().Patch(ctx, environment, envPatch)

	if err != nil {
		environment.Status.Message = err.Error()
		_ = r.Status().Patch(ctx, environment, envPatch)
		return ctrl.Result{}, errors.Wrap(err, "could not update Environment status after sync")
	}

	// final return, requeue
	return ctrl.Result{RequeueAfter: requeueInterval}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *EnvironmentReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vf1a2.Environment{}).
		Complete(r)
}
