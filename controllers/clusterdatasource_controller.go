/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package controllers

import (
	"context"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a2 "gitlab.com/jrr/vault-friend/api/v1alpha2"
)

// ClusterDataSourceReconciler reconciles a ClusterDataSource object
type ClusterDataSourceReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=vault-friend.jrr.io,resources=clusterdatasources,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=vault-friend.jrr.io,resources=clusterdatasources/status,verbs=get;update;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *ClusterDataSourceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := r.Log.WithValues("clusterdatasource", req.NamespacedName)
	logger.Info("reconciling ClusterDataSource")

	clusterDataSource := &vf1a2.ClusterDataSource{}
	err := r.Get(ctx, req.NamespacedName, clusterDataSource)

	if kerrors.IsNotFound(err) {
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not fetch ClusterDataSource")
	}

	if !clusterDataSource.ObjectMeta.DeletionTimestamp.IsZero() {
		// object is being deleted, nothing to do
		return ctrl.Result{}, nil
	}

	vaultClient, err := clusterDataSource.Spec.CreateClient()

	if err != nil {
		logger.Error(err, "error creating Vault client")
		return ctrl.Result{}, errors.Wrap(err, "could not create client for ClusterDataSource")
	}

	timeNow := metav1.Now()
	clusterDataSource.Status.LastTouch = &timeNow
	clusterDataSource.Status.Connected = false
	clusterDataSource.Status.Message = ""

	sealStatus, sealErr := vaultClient.Sys().SealStatus()

	if sealErr != nil {
		clusterDataSource.Status.Message = sealErr.Error()
	} else {
		clusterDataSource.Status.Connected = true
		clusterDataSource.Status.Unsealed = !sealStatus.Sealed
		clusterDataSource.Status.Message = "ok"
	}

	err = r.Status().Update(ctx, clusterDataSource)

	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "could not update ClusterDataSource status")
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ClusterDataSourceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&vf1a2.ClusterDataSource{}).
		Complete(r)
}
