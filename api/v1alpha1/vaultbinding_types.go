/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// VaultBindingSpec defines the desired state of VaultBinding
type VaultBindingSpec struct {
	// VaultName is the cluster-level Vault object from which Secrets will
	// be pulled.
	// +required
	VaultName string `json:"vaultName,omitempty"`

	// VaultRole is the Vault Kubernetes auth engine role that should be
	// used to authenticate and authorize this binding.
	// +required
	VaultRole string `json:"vaultRole,omitempty"`

	// TargetRef points to the Pod-generating controller to which
	// the Secrets should be bound.
	// +required
	TargetRef TargetReference `json:"targetRef,omitempty"`

	// Resources are the Vault resources (Vault{Certificate,Environment})
	// that will be bound to the TargetRef.
	// +kubebuilder:validation:MinItems=1
	// +required
	Resources []VaultResource `json:"resources,omitempty"`
}

// ResourceKind is a constant over string that specifies the type of an
// attached VaultResource.
// +kubebuilder:validation:Enum=VaultEnvironment
type ResourceKind string

const (
	ResourceKindCert ResourceKind = "VaultCertificate"
	ResourceKindEnv  ResourceKind = "VaultEnvironment"
)

// TargetKind is a constant over string that specifies the supported target types.
// +kubebuilder:validation:Enum=CronJob;DaemonSet;Deployment;Job;StatefulSet
type TargetKind string

const (
	TargetKindCronJob     TargetKind = "CronJob"
	TargetKindDaemonSet   TargetKind = "DaemonSet"
	TargetKindDeployment  TargetKind = "Deployment"
	TargetKindJob         TargetKind = "Job"
	TargetKindStatefulSet TargetKind = "StatefulSet"
)

// TargetReference is a simplified reference to a Pod-generating controller.
type TargetReference struct {
	// Kind is the kind of the target controller.
	// +required
	Kind TargetKind `json:"kind,omitempty"`

	// Name is the .metadata.name of the target.
	// +required
	Name string `json:"name,omitempty"`

	// ContainerNames tells Vault-Friend which containers should have
	// secret attached to them. Operates as a whitelist if non-empty.
	// +optional
	ContainerNames []string `json:"containerNames,omitempty"`
}

// VaultResource is an association of a Vault resource to a Target.
type VaultResource struct {
	// Kind specifies the type of the associated Vault resource.
	// +required
	Kind ResourceKind `json:"kind,omitempty"`

	// Name is the .metadata.name of the target Vault resource.
	// +required
	Name string `json:"name,omitempty"`

	// DEPRECATED: MountPath specifies where Vault-Friend should put a
	// VaultCertificate's secret data. Ignored if provided for
	// VaultEnvironments as they are not mounted in the filesystem.
	// +optional
	MountPath string `json:"mountPath,omitempty"`
}

// VaultBindingStatus is the observed state of VaultBinding.
type VaultBindingStatus struct {
	// OK indicates whether the overall binding is ready to be attached
	// to a Pod.
	// +required
	OK bool `json:"ok,omitempty"`

	// Message is a human-readable status indication.
	// +optional
	Message string `json:"message,omitempty"`

	// Resources is the last observed state of each resource.
	Resources []ResourceStatus `json:"resources,omitempty"`
}

// ResourceStatus is the observed state of a resource.
type ResourceStatus struct {
	// Name is the name of the associated Vault resource.
	// +required
	Name string `json:"name,omitempty"`

	// Kind specifies the type of the associated Vault resource.
	// +required
	Kind ResourceKind `json:"kind,omitempty"`

	// OK indicates whether the secret is ready to be attached to a Pod.
	// +required
	OK bool `json:"ok,omitempty"`

	// Message is a human-readable status indication.
	// +optional
	Message string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=vb

// VaultBinding attaches a series of Vault resources to a
// Pod-generating controller.
// +kubebuilder:printcolumn:name="OK",type=boolean,JSONPath=`.status.ok`
type VaultBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultBindingSpec   `json:"spec,omitempty"`
	Status VaultBindingStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// VaultBindingList contains a list of VaultBinding
type VaultBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VaultBinding `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VaultBinding{}, &VaultBindingList{})
}
