/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// VaultSpec is a connection to a Vault server
type VaultSpec struct {
	// Address is the fully-qualified URL of the Vault server from e.g.,
	// VAULT_URL environment variables.
	// +required
	Address string `json:"address,omitempty"`

	// AuthPath is the path at which your Kubernetes auth engine is
	// mounted.
	// +required
	AuthPath string `json:"authPath,omitempty"`

	// CACert is a base64-encoded CA certificate PEM that signs the
	// Vault server's certificate at the given Address.
	// +optional
	CACert string `json:"caCert,omitempty"`
}

// VaultStatus is the status of the Vault server connection
type VaultStatus struct {
	// Connected indicates whether the controller can connect to the
	// given Vault server's /v1/sys/seal-status and receive a 200-class error.
	Connected bool `json:"connected"`

	// Sealed indicates whether the Vault server says that it is sealed.
	// Sealed Vaults cannot be pulled from.
	Sealed bool `json:"sealed"`

	// Message is a human-readable indication of the status of the
	// connection to the Vault server.
	// +optional
	Message string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope=Cluster

// Vault defines a reusable connection to a Vault server
// +kubebuilder:printcolumn:name="Connected",type=boolean,JSONPath=`.status.connected`
type Vault struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultSpec   `json:"spec,omitempty"`
	Status VaultStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// VaultList contains a list of Vault
type VaultList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Vault `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Vault{}, &VaultList{})
}
