/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// VaultEnvironmentSpec defines the desired state of the VaultEnvironment
type VaultEnvironmentSpec struct {
	// Path is the Vault KV path to the desired environment variables.
	// +required
	Path string `json:"path,omitempty"`

	// AlwaysUpdate is a flag to disable version checking.
	// Defaults to `false` if unset.
	// If false, the Vault metadata will be checked against
	// .status.version and only update the Secret if it differs.
	// +optional
	AlwaysUpdate bool `json:"alwaysUpdate,omitempty"`

	// PinVersion forces a particular version of a KV v2 path to be used.
	// Defaults to `0`. Overrides AlwaysUpdate if non-zero.
	// +optional
	PinVersion int `json:"pinVersion,omitempty"`

	// EnvPrefix adds a string prefix to each key in the path.
	// This happens before keys would be capitalized by CapitalizeKeys.
	// +optional
	EnvPrefix string `json:"envPrefix,omitempty"`

	// CapitalizeKeys is a flag to transform all keys into ENV VAR STYLE.
	// Defaults to `false` if unset.
	// +optional
	CapitalizeKeys bool `json:"capitalizeKeys,omitempty"`
}

// VaultEnvironmentStatus is the state of the latest attempt to fetch the path.
type VaultEnvironmentStatus struct {
	// OK indicates that the environment was successfully fetched.
	OK bool `json:"ok,omitempty"`

	// Message is a human-readable status indication.
	// +optional
	Message string `json:"message,omitempty"`

	// Version is the last fetched KV v2 secret version.
	Version int `json:"version,omitempty"`

	// LastFetchTime is the last time the environment was fetched.
	LastFetchTime *metav1.Time `json:"lastFetchTime,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=ve;vaultenv

// VaultEnvironment is a Vault KV path to use as pod environment variables.
// +kubebuilder:printcolumn:name="OK",type=boolean,JSONPath=`.status.ok`
// +kubebuilder:printcolumn:name="Stored Version",type=integer,JSONPath=`.status.version`
// +kubebuilder:printcolumn:name="Last Fetched",type=string,format=date-time,JSONPath=`.status.lastFetchTime`
type VaultEnvironment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultEnvironmentSpec   `json:"spec,omitempty"`
	Status VaultEnvironmentStatus `json:"status,omitempty"`
}

func (env *VaultEnvironment) NeedsUpdate() bool {
	return !(env.Status.OK) || env.Spec.AlwaysUpdate || (env.Spec.PinVersion != 0 && (env.Status.Version != env.Spec.PinVersion))
}

// +kubebuilder:object:root=true

// VaultEnvironmentList contains a list of VaultEnvironment
type VaultEnvironmentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VaultEnvironment `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VaultEnvironment{}, &VaultEnvironmentList{})
}
